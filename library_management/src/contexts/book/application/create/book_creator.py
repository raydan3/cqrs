from src.contexts.book.domain.book_repository import BookRepository
from src.contexts.book.domain.book import Book
from src.contexts.book.domain.value_object.book_id import BookId
from src.contexts.book.domain.value_object.book_isbn import BookIsbn
from src.contexts.book.domain.value_object.book_title import BookTitle
from src.contexts.book.domain.value_object.book_author import BookAuthor
from src.contexts.book.domain.value_object.book_category import BookCategory


class BookCreator:

    def __init__(self, repository: BookRepository) -> None:
        self._repository = repository

    async def save(self, id: str, isbn: str, title: str, author: str,
                   category: str) -> Book:
        book = Book(BookId(id), BookIsbn(isbn), BookTitle(title),
                    BookAuthor(author), BookCategory(category))

        await self._repository.save(book)
        return book
