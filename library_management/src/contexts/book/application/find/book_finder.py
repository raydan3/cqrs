from typing import Optional

from src.contexts.book.domain.exception.book_not_found import BookNotFoundException
from src.contexts.book.domain.value_object.book_id import BookId
from src.contexts.book.domain.book import Book
from src.contexts.book.domain.book_repository import BookRepository


class BookFinder:

    def __init__(self, repository: BookRepository) -> None:
        self._repository = repository

    async def find(self, id: str) -> Optional[Book]:
        book = await self._repository.find(BookId(id))
        if not book:
            raise BookNotFoundException
        return book
