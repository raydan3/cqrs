from src.contexts.shared.domain.value_object.uuid import Uuid


class BookId(Uuid):
    pass
