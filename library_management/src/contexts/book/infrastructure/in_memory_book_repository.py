from typing import Dict, Optional

from src.contexts.book.domain.book_repository import BookRepository
from src.contexts.book.domain.book import Book
from src.contexts.book.domain.value_object.book_id import BookId


class InMemoryBookRepository(BookRepository):

    def __init__(self, catalog: Optional[Dict] = None) -> None:
        if catalog is None:
            catalog = {}
        self._catalog = catalog

    async def save(self, book: Book) -> None:
        self._catalog[book.id.value] = book

    async def find(self, id: BookId) -> Optional[Book]:
        return self._catalog.get(id.value)
