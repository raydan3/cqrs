from dataclasses import dataclass


@dataclass(frozen=True)
class IntValueObject:
    _value: int

    @property
    def value(self) -> int:
        return self._value
